#!/usr/bin/python

import csv
import genanki

DECK_ID = 1940947845
MODEL = genanki.Model(
    1480130210,
    'Fundamento Model',
    fields=[
        {'name': 'Question'},
        {'name': 'Answer'}
    ],
    templates=[
        {
            'name': 'Card 1',
            'qfmt': '<b>{{Question}}</b>',
            'afmt': '{{FrontSide}}<hr id="answer">{{Answer}}'
        }
    ])


def add_custom_note(deck, fields):
    deck.add_note(genanki.Note(model=MODEL, fields=fields))


def make_deck(reader):
    versions = [reader.fieldnames[1:]]
    version_names = []

    for i in range(1, len(reader.fieldnames)):
        versions.append([reader.fieldnames[i]])

    for version in versions:
        version_names.append(','.join(version))

    decks = []

    for idx in range(len(versions)):
        decks.append(genanki.Deck(
            DECK_ID + idx,
            'Fundamento de Esperanto (' + version_names[idx] + ')'
        ))

    for row in reader:
        for idx in range(len(versions)):
            translated = ''
            for field in versions[idx]:
                translated += '<b>' + field + '</b>: ' + row[field] + '<br/>'

            fields = ['EO:' + row['EO'], translated]
            add_custom_note(decks[idx], fields)
            add_custom_note(decks[idx], [fields[1], fields[0]])

    for idx in range(len(versions)):
        genanki.Package(decks[idx]).write_to_file(version_names[idx] + '.apkg')


with open('word_list.csv') as csvfile:
    make_deck(csv.DictReader(csvfile, delimiter='|'))
