# Anki Fundamento de Esperanto

Software to generate Anki flashcards from the book Fundamento de Esperanto, in five languages.

You can download the .apkg files from [releases](https://gitlab.com/hendursaga/anki-fundamento/-/releases) or from [AnkiWeb](https://ankiweb.net/shared/byauthor/1622379115), although some of the decks from the latter might expire.

Feel free to rate the decks and star this repository!
